# AppiumAir
<a href="javascript:void(0)"><img src="https://gitee.com/songjianghu/AppiumAir/raw/master/src/main/resources/ico/AppiumAir.png"/></a>
## 项目介绍
AppiumAir是基于Appium二次封装的Android自动化框架，多线程方式在多台手机上同时执行测试，自动获取已连接各设备信息，自动启动已连接设备相同多个appium服务，测试用例可按模块划分并顺序执行，多个测试用例可顺序连续执行，无需重启APP，用例执行失败自动截图、录制视频、收集报错信息，列表中高亮显示，全新报告模板，内容全面，样式新颖，手机连接可直接运行

[![](https://img.shields.io/badge/RELEASE-v3.1-red.svg)](https://gitee.com/songjianghu/AppiumAir/repository/archive/v3.1)
[![](https://img.shields.io/badge/CHAT-QQ-blue.svg)](https://qm.qq.com/cgi-bin/qm/qr?k=VLN_IPf76i_rfgzXOMhDc81xa7TrqFbz&jump_from=webapi)
![](https://img.shields.io/badge/Gitee_Scan-A-success.svg)
![](https://img.shields.io/badge/Maven-3.x-green.svg)
![](https://img.shields.io/badge/Android-4.4+-green.svg)
![](https://img.shields.io/badge/JDK-11+-green.svg)
![](https://img.shields.io/badge/License-Apache%202.0-green.svg)

## Gitee Pages
- 测试报告演示：https://songjianghu.gitee.io/appiumair
- 项目介绍页面：https://songjianghu.github.io/AppiumAir

## QQ交流群
- QQ群：774757006
- 觉得很好用? 给个 ****Star****，就是对作者最大的鼓励
- 遇到解决不了的问题? 需要特殊的功能却没有? 有更好的建议? 请加群讨论

## 主要功能
- 多线程方式在多台手机上同时执行测试，大幅提高测试效率
- 自动获取已连接各设备信息，自动启动已连接设备相同多个appium服务
- 多个测试用例连续执行，无需重新启动APP，缩短测试用例执行间隔
- 测试用例无需配置，自动扫描执行，让测试人员更加专注业务逻辑
- 用例执行失败自动截图、录制视频、收集报错信息，列表中高亮显示
- 使用ExtentReport报告插件二次改造，内容全面，样式新颖

## 框架应用
- 兼容性测试：每条用例均可覆盖成百上千款主流机型
- 回归测试：上线前自动化验证业务关键流程
- 负载测试：可模拟大量真机各种场景下负载测试
- 线上监控：线上实时监控业务可用状态，异常报警
- 其他场景：真机模拟并发抢购、真机模拟DDoS攻击等场景

## 环境配置
- 安装JDK8或以上，并配置JAVA_HOME环境变量
```
JAVA_HOME=C:\Program Files\Java\jdk1.8.0_141
PATH=%JAVA_HOME%\bin
```
- 安装SDK，并配置环境变量
```
ANDROID_HOME=D:\software\android-sdk-windows(SDK安装目录)
PATH=%ANDROID_HOME%\platform-tools
PATH=%ANDROID_HOME%\tools
PATH=%ANDROID_HOME%\build-tools\30.0.2
```

- 安装Maven3
```
MAVEN_HOME=D:\develop\apache-maven-3.6.3(Maven安装目录)
PATH=%MAVEN_HOME%\bin
```

- 配置setting.xml
```
<mirror>
    <id>aliyunmaven</id>
    <mirrorOf>*</mirrorOf>
    <name>阿里云公共仓库</name>
    <url>https://maven.aliyun.com/repository/public</url>
</mirror>
```

- 安装Node.js，windows系统不要安装在C盘(系统盘)，根据提示安装即可
- 下载地址：https://nodejs.org/en/
- 配置node全局模块安装目录，在node.js安装目录下新建两个文件夹node_global和node_cache，然后在cmd(管理员)命令下执行如下两个命令：
```
  npm config set prefix "D:\app\nodejs\node_global"
  npm config set cache "D:\app\nodejs\node_cache"
```

- 配置node.js环境变量
```
PATH=D:\app\nodejs
NODE_PATH=D:\app\nodejs\node_modules
PATH=D:\app\nodejs\node_global
PATH=D:\app\nodejs\node_modules\npm
```
- 安装Appium server
- 安装前测试一下默认地址是否可以访问：https://registry.npmjs.org/appium
- 网络较好，使用npm安装：
```
npm install -g appium
```
- 网络一般，可以使用cnpm安装appium server，需要先安装cnpm
```
npm install -g cnpm --registry=https://registry.npm.taobao.org
cnpm install -g appium
```

- 连接正常，第一次无法启动手机，可能是手机没有安装appium-uiautomator2-server-v-0.1.x.apk导致的，请下apk安装到手机
```
https://github.com/appium/appium-uiautomator2-server/releases/tag/
```

- 手机开启开发者模式，开启USB调试，开启USB安装，开启USB调试(安全设置)
- 下载apk安装包，下载地址：
```
https://market.m.taobao.com/app/fdilab/download-page/main/index.html
```
- 获取appPackage、appActivity，使用aapt命令从apk安装包中获取，找到package:name和launchable-activity: name
```  
aapt dump badging D:/app/com.taobao.taobao.apk
```

- 使用数据线连接手机：选择USB用途-->传输文件
- 测试手机是否连接成功
```
adb devices
```

- 开发工具： 使用IEDA或者Eclipse都可以

- IDEA下载地址：
```
https://www.jetbrains.com/idea/
```
- Eclipse下载地址
```
https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2021-03/R/eclipse-java-2021-03-R-win32-x86_64.zip
```

## 使用说明
- 代码仓库：
```
https://gitee.com/songjianghu/AppiumAir.git
```
- 检出代码： 
```
  git clone https://gitee.com/songjianghu/AppiumAir.git
```
- 代码启动入口：AppiumTest
- APP配置文件：resources/config/config.properties
- 其他说明文档：在resources/doc目录下
- 测试报告位置：Spark/index.html

## 测试用例编写规范：
- 测试用例请放com.appiumair.testcase包下，以子包的形式存在，不支持在testcase包下直接创建测试用例，这样将不仅无法扫描到用例，并且执行将会报错，请注意一下。
- 用例如何划分模块呢? 请在testcase包下新建子包，建议根据业务功能、服务模块等进行划分，包命名请使用p01_product、p02_order等形式进行命名，这样可以根据包名顺序执行，如果不进行编号，则随机执行
- 测试用例如何命名呢? 建议通过TC01_TBIndexSearch、TC02_TBIndexBrowser等形式命名测试用例，这样可以根据用例编号顺序执行用例，如果不编号，则随机执行
- 如何编写一个标准的测试用例呢? 只需要继承ExtentReport类，实现TestCase接口，重写run方法即可
- 暂时不支持根据机器配置自动设置线程池大小，防止并发过多造成机器卡顿、死机问题，正在优化中，请按需增加配置，如增加内存、增加CPU核心数、升级硬盘等

## 环境配置参考
- 如果环境配置搞不定，可以参考下面链接的博客
```
https://my.oschina.net/outcat/blog/491529
```

## Web自动化Selenium：docker分布式UI自动化部署指南
## 环境准备
- 1、准备一台CentOS 7主机(带界面，后面有用)，最小配置：1核1G，磁盘空间20GB以上，建议配置：4核8G，磁盘空间：40GB以上
- 2、Linux上安装docker，docker官方安装地址参考：
```
https://docs.docker.com/engine/install/centos/
```
- 3、主要步骤如下：
```
# 卸载老的版本：
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine

# 安装管理repository及扩展包的工具 (主要是针对repository)
sudo yum install -y yum-utils

# 设置仓库
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

# 安装 Docker Engine
sudo yum install docker-ce docker-ce-cli containerd.io

# To install a specific version of Docker Engine, list the available versions in the repo, then select and install:
# yum list docker-ce --showduplicates | sort -r

# 启动docker
sudo systemctl start docker

# 测试docker是否安装成功
sudo docker run hello-world
```

## 获取docker镜像
```
# 在docker hub中搜索selenium镜像
docker search selenium

# 下载五个镜像：一个hub镜像，两个Chrome浏览器镜像，两个Firefox浏览器镜像
# docker pull selenium/hub
# docker pull selenium/node-firefox-debug
# docker pull selenium/node-chrome-debug

docker pull baozhida/selenium-hub:3.3.1
docker pull baozhida/selenium-node-chrome-debug:48
docker pull baozhida/selenium-node-chrome-debug:58
docker pull baozhida/selenium-node-firefox-debug:47
docker pull baozhida/selenium-node-firefox-debug:52

# 启动docker hub
docker run -p 5555:4444 -d --name hub baozhida/selenium-hub:3.3.1

# 启动四个浏览器容器
docker run -d -p 5911:5900 --link hub:hub --name chrome58 baozhida/selenium-node-chrome-debug:58
docker run -d -p 5912:5900 --link hub:hub --name chrome48 baozhida/selenium-node-chrome-debug:48
docker run -d -p 5921:5900 --link hub:hub --name firefox47 baozhida/selenium-node-firefox-debug:47
docker run -d -p 5922:5900 --link hub:hub --name firefox52 baozhida/selenium-node-firefox-debug:52

# 看一下是否有五个正在运行的容器
docker ps
```

- 通过浏览器查看，应该可以看到四个浏览器对象
- http://localhost:5555/grid/console

## 安装 VNC Viewer
- 下载地址：https://www.realvnc.com/en/connect/download/viewer/linux/
- 下载版本：Linux下的RPM x64版本
- 下载完安装即可，安装完毕，启动:
- file-->new connect：VNC Server里面输入：localhost:5911
- file-->new connect：VNC Server里面输入：localhost:5912
- file-->new connect：VNC Server里面输入：localhost:5921
- file-->new connect：VNC Server里面输入：localhost:5922
- 在VNC Viewer里面双击打开已经创建好的两个主机
- 输入密码：secret

## 浏览器历史版本参考
- Firefox：https://www.mozilla.org/en-US/firefox/releases/
- Chrome：https://chromedriver.chromium.org/downloads

## 博客参考
- https://www.cnblogs.com/aeip/p/9480099.html
- https://www.cnblogs.com/nanaheidebk/p/10109013.html
- https://blog.csdn.net/qq_37688023/article/details/105891378

## 那些公司在使用


## License
AppiumAir采用Apache-2.0开源许可进行编写。

## 开源支持
<a href="https://www.songjianghu.com"><img src="https://gitee.com/songjianghu/AppiumAir/raw/master/src/main/resources/ico/jetbrains.jpg" width="100" heith="100"/></a>