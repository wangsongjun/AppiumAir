package com.appiumair.po;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import java.util.concurrent.TimeUnit;

/**
 * PO模型案例，每个页面中的元素封装成一个PO模型
 */
public class P_HomePage {

    public static void homePageDoSearchByKey(AndroidDriver driver, String key) throws InterruptedException {
        driver.findElement(By.id("com.taobao.taobao:id/searchEdit")).sendKeys(key);
        TimeUnit.SECONDS.sleep(1);
        driver.findElement(By.id("com.taobao.taobao:id/searchbtn")).click();
        TimeUnit.SECONDS.sleep(1);
    }

}
