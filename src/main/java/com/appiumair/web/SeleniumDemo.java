package com.appiumair.web;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 需求：一个用例同时在chrome浏览器不同版本、Firefox浏览器不同版本上运行
 */
public class SeleniumDemo {

    public static void browser(String browser, String version) {
        DesiredCapabilities cap = new DesiredCapabilities(browser, version, Platform.LINUX);
        cap.setJavascriptEnabled(true);
        WebDriver driver = null;

        try {
            driver = new RemoteWebDriver(new URL("http://192.168.0.103:5555/wd/hub"), cap);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.get("https://gitee.com/songjianghu/AppiumAir");
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //WebDriver augmentedDriver = new Augmenter().augment(driver);
        //File screenshot = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
        driver.close();
        driver.quit();
    }

    public static void main(String[] args) throws MalformedURLException {

        ExecutorService threadpool = Executors.newFixedThreadPool(5);

        Map<String, String> map = new HashMap<>();
        // 浏览器版本一定要写全写对
        map.put("58.0.3029.81", "chrome");
        map.put("48.0.2564.109", "chrome");
        //map.put("47.0.2", "firefox");
        map.put("52.0", "firefox");

        for(Map.Entry<String, String> entry : map.entrySet()){
            String browse = entry.getKey();
            String version = entry.getValue();

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    browser(version, browse);
                }
            };
            threadpool.submit(runnable);
        }

    }

//    ChromeOptions chromeOptions = new ChromeOptions().merge(desiredCapabilities);
//        chromeOptions.addArguments("--no-sandbox");
//    HashMap<String, Object> hashMap = new HashMap<>();
//        hashMap.put("download.default_directory", "指定下载路径字符串");
//        chromeOptions.setExperimentalOption("prefs", hashMap);
//    //创建远程driver对象
//    WebDriver driver=new RemoteWebDriver(new URL("http://ip地址:9012/wd/hub/"), chromeOptions);

}
